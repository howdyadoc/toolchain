# Howdyadoc toolchain

## 1. Installation

### 1.1. Linux

#### 1.1.1. Debian package installation

Download the latest deb package here: <https://gitlab.com/howdyadoc/toolchain/-/releases/>

Then install it with

```bash
sudo apt install ./howdyadoc_*_amd64.deb
```

(mind the `./` before the debian package filename; and do *not* try to install it with `dpkg -i`)

If you want to use **[Atom](https://atom.io/) as an editor for the howdyadoc workflow (highly recommended)**, you can locally configure it (as user) with

```bash
howdyadoc-configure-atom
```

Please note that you must have Atom **already installed** and must have **run it** at least once.

This command will install and configure all required packages (and configure your git user name and email, if not already set), so you can start using the howdyadoc workflow with Atom.

#### 1.1.2. Manual Installation

Requirements:

- **Libreoffice**: installing Libreoffice is not strictly required (apart from `docx2pdf` command) but highly recommended.
- **mustache**: any full implementation of mustache specs should work
- **python3-cson**: this is needed just by `howdyadoc-configure-atom` (the command that locally configures atom as an editor for the howdyadoc workflow).
- **git**: this is needed for the howdyadoc workflow, and for some atom plugins installed by `howdyadoc-configure-atom`.
- **make**: this is needed for the howdyadoc workflow

On debian-based systems (Debian, Ubuntu, Mint...), you can install all requirements with

```bash
sudo apt install -y libreoffice-writer ruby-mustache python3-cson git make
```

After installing requirements, just do:

```bash
cd src/linux/howdyadoc-1.0
make
sudo make install
```
This will download pandoc and pandoc-crossref binaries, install howdyadoc in `/usr/local/lib` and create links to `howdyadoc-preview`, `howdyadoc-convert` and `docx2pdf` in `/usr/local/bin`

To uninstall, do

```bash
cd src/linux/howdyadoc-1.0
sudo make uninstall
```

## 2. Usage

TODO
